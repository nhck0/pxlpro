$(function () {
    var navList = $('.navigation-list__item');

    navList.click(function () {
        $(this).addClass('navigation-list__item--active')
            .siblings().removeClass('navigation-list__item--active')
    })
});