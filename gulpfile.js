var gulp = require('gulp'),
	sass = require('gulp-sass'),
	pug = require('gulp-pug'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
    spritesmith = require('gulp.spritesmith'),
	notify = require('gulp-notify'),
	clean = require('gulp-clean'),
	uglify = require('gulp-uglify'),
	wiredep = require('gulp-wiredep'),
	useref = require('gulp-useref'),
	cssnano = require('gulp-cssnano'),
	browserSync = require('browser-sync').create();
var path = {
    build: {
    	html: 'build/',
        js: 'build/js/',
        styles: 'build/css/',
        assets: 'build/assets/',
        fonts: 'build/fonts/'
    },
    src: { 
        html: 'src/template/index.pug',
        js: 'src/js/*.js',
        styles: 'src/styles/main.scss',
        assets: 'src/assets/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: { 
        html: 'src/**/*.pug',
        js: 'src/js/**/*.js',
        styles: 'src/styles/**/*.{css,scss}',
        assets: 'src/assets/**/*.*',
        fonts: 'src/fonts/**/*.*',
		bower: './bower.json'
    },
	clean: './build'
};

gulp.task('default', ['clean'], function(){
	gulp.run('dev')
});

gulp.task('production', ['clean'], function(){
	gulp.run('BuildProd')
});

gulp.task('BuildProd', ['buildPug', 'buildCSS', 'sprite', 'buildFonts']);

gulp.task('dev', ['build', 'watch', 'browser-sync']);

gulp.task('build', ['buildPug','buildCSS', 'sprite', 'buildFonts']);

gulp.task('watch', function() {
	gulp.watch(path.watch.styles, ['buildCSS']);
	gulp.watch(path.watch.js, ['HTML']);
	gulp.watch(path.watch.fonts, ['buildFonts']);
    gulp.watch(path.watch.html, ['buildPug']);
    gulp.watch(path.watch.assets, ['buildAssets']);
});

gulp.task('buildPug', function(){
	gulp.src(path.src.html)
	.pipe(plumber({
        errorHandler: notify.onError(function(err) {
            return {
                title: 'PUG',
                message: err.message
            }
        })
    }))
	.pipe(pug({
		pretty: true
	}))
	.pipe(gulp.dest(path.build.html))
    .on('end', function () {
        gulp.run('buildHTML');
    })
    .pipe(browserSync.reload({stream: true}));
});

gulp.task('HTML', function(){
	gulp.src('build/index.html')
	.pipe(wiredep({
		directory: 'bower_components/'
	}))
	.pipe(useref())
	.pipe(gulp.dest(path.build.html))

});

gulp.task('buildHTML', function(){
	gulp.src('build/index.html')
	.pipe(wiredep({
		directory: 'bower_components/'
	}))
	.pipe(useref())
	.pipe(gulp.dest(path.build.html))
	.on('end', function(){
		gulp.run('minCSS')
		gulp.run('minJS')
	})

});

gulp.task('minCSS', function(){
	gulp.src('build/css/vendor.css')
	.pipe(cssnano())
	.pipe(gulp.dest(path.build.styles))
});

gulp.task('minJS', function(){
	gulp.src('build/js/*.js')
	.pipe(uglify())
	.pipe(gulp.dest(path.build.js))
});

gulp.task('scripts', function() {
	gulp.src(path.src.js)
		.pipe(uglify())
		.pipe(gulp.dest('build/js'));
});

gulp.task('buildCSS', function(){
	gulp.src(path.src.styles)
	.pipe(plumber({
			errorHandler: notify.onError(function(err) {
				return {
					title: 'Styles',
					message: err.message
				}
			})
		}))
	.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
		.pipe(concat('main.css'))
		.pipe(cssnano())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest(path.build.styles))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('buildFonts', function(){
    gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

gulp.task('clean', function(){
	return gulp.src(path.clean)
	.pipe(clean())
});

gulp.task('sprite', function() {
    var spriteData = gulp.src('src/assets/sprite/*.png')
		.pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '../assets/sprite/sprite.png',
        cssName: 'sprite.scss'
    }));

    spriteData.img.pipe(gulp.dest('src/assets/sprite'));
    spriteData.css.pipe(gulp.dest('src/styles/'))

    .on('end', function () {
		gulp.run('buildAssets')
    })
});

gulp.task('buildAssets', function(){
    gulp.src(path.src.assets)
        .pipe(gulp.dest(path.build.assets))
});

gulp.task('browser-sync', function() {
	browserSync.init({
		server:{
			baseDir: './build'
		}
	});
});